export class NoteItem {

  public readonly id: string;

  constructor(
    public readonly author: string,
    public content: string = '',
    public readonly creationTime: Date = new Date(),
  ) {
    this.id = this.createUUID();
  }

  private createUUID() : string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}

// The NoteItem as JSON - contains all the NoteItem properties as strings
export type NoteJSON = {[P in keyof NoteItem]: string};
