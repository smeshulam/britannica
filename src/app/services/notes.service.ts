import {Injectable} from '@angular/core';
import {NoteItem, NoteJSON} from '../models/NoteItem';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  private readonly LS_NAME: string = 'britannicaAPP';

  private _notes: NoteItem[] = this.readItems();

  get notes(): NoteItem[] {
    return [...this._notes];
  }

  constructor() { }

  createNote(author: string, content: string) {
    const note = new NoteItem(author, content);
    this._notes.push(note);
    this.saveItems();
    return note;
  }

  updateNote(id: string, newContent: string) {
    const note = this._notes.find(n => n.id === id);
    if (note) {
      note.content = newContent;
      this.saveItems();
      return note;
    }
  }

  deleteNote(id: string) {
    const idx = this._notes.findIndex(n => n.id === id);
    this._notes.splice(idx, 1);
    this.saveItems();
  }

  private saveItems() {
    localStorage.setItem(this.LS_NAME, JSON.stringify(this._notes));
  }

  private readItems(): NoteItem[] {
    const str = localStorage.getItem(this.LS_NAME);
    if (str) {
      const list = JSON.parse(str) as NoteJSON[];
      return list.map(item => new NoteItem(item.author, item.content, new Date(item.creationTime)));
    } else {
      return [];
    }
  }

}
