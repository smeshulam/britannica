import {NgModule} from '@angular/core';
import {BoardComponent} from './board.component';
import {NoteComponent} from './components/note/note.component';
import {NoteModalComponent} from './components/note-modal/note-modal.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    BoardComponent,
    NoteComponent,
    NoteModalComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([{path: '', component: BoardComponent}]),
  ]
})
export class BoardModule { }
