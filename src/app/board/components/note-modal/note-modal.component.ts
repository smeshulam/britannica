import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NoteItem} from '../../../models/NoteItem';
import {NotesService} from '../../../services/notes.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-note-modal',
  templateUrl: './note-modal.component.html',
  styleUrls: ['./note-modal.component.scss']
})
export class NoteModalComponent implements OnInit, AfterViewInit {

  @ViewChild('f') form?: NgForm;

  isNew = false;

  editMode = false;

  constructor(
    private notesService: NotesService,
    public dialogRef: MatDialogRef<NoteModalComponent>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public note: NoteItem
  ) {}

  ngOnInit() {
    this.isNew = !this.note;
  }

  ngAfterViewInit(): void {
    if (!this.isNew && this.form) {
      setTimeout(() => {
        this.form?.setValue({
          author: this.note?.author,
          content: this.note?.content,
        });
      });
    }
  }

  submit(f: NgForm) {
    if (this.isNew) {
      this.notesService.createNote(f.value.author, f.value.content);
    } else if (this.note) {
      this.notesService.updateNote(this.note.id, f.value.content);
    }
    this.dialogRef.close();
    this.snackBar.open(this.isNew ? 'Note added!' : 'Note updated!', '', {duration: 3000, panelClass: 'success-msg'});
  }

  delete() {
    if (this.note) {
      this.notesService.deleteNote(this.note.id);
      this.snackBar.open('Note deleted!', '', {duration: 3000, panelClass: 'delete-msg'});
      this.dialogRef.close();
    }
  }

}
