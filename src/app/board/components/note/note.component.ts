import {Component, Input, OnInit} from '@angular/core';
import {NoteItem} from '../../../models/NoteItem';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {

  @Input() note?: NoteItem;

  constructor(
  ) { }

  ngOnInit(): void {
  }

}
