import {Component, OnInit} from '@angular/core';
import {NotesService} from '../services/notes.service';
import {NoteItem} from '../models/NoteItem';
import {MatDialog} from '@angular/material/dialog';
import {NoteModalComponent} from './components/note-modal/note-modal.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  get notes(): NoteItem[] {
    return this.notesService.notes;
  }

  constructor(
    private notesService: NotesService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  openModal(note?: NoteItem) {
    this.dialog.open(NoteModalComponent, {
      hasBackdrop: true,
      disableClose: true,
      width: '100vw',
      maxWidth: '1000px',
      data: note,
    });
  }

}
