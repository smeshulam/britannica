import { Pipe, PipeTransform } from '@angular/core';
import {NoteItem} from '../models/NoteItem';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: NoteItem[]): NoteItem[] {
    return value.sort(((a, b) => b.creationTime.getTime() - a.creationTime.getTime()));
  }

}
